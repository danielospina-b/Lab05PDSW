/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.samples.tests;

import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.services.ExcepcionServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquilerItemsStub;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CE1: Se debe agregar un cliente  con todos los datos completos normalmente. TIPO: Normal. 
 * Resultado Esperado: El cliente existe en la lista de clientes
 * 
 * CE2: Se debe agregar un cliente con datos en sus campos = null. TIPO: Frontera.
 * Resultado Esperado: El objeto cliente sebe estar en la lista de clientes
 * 
 */
public class ClientesTest {

    public ClientesTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void testCE1() {
        ServiciosAlquilerItemsStub servicios = new ServiciosAlquilerItemsStub();
        Cliente cliente = new Cliente("Juan Camilo Ramirez", 1015354786, "3214896", "Cll73#45-21", "juancramirez@yopmail.com");
        try {
            servicios.registrarCliente(cliente);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("No se pudo ingresar el cliente, lanzo excepcion");
        }
        Cliente res = null;
        try {
            res = servicios.consultarCliente(1015354786);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("Fallo al consultar el cliente");
        }
        assertEquals("Comprobando clientes iguales", cliente, res);
    }   
    
    @Test
    public void testCE2() {
        try {
            ServiciosAlquilerItemsStub servicios = new ServiciosAlquilerItemsStub();
            long id = 1;
            int tamanoInicial = servicios.consultarClientes().size();
            Cliente cliente = new Cliente(null, id, null, null, null);
            servicios.registrarCliente(cliente);
            assertTrue("Se agregó el cliente a la lista", servicios.consultarClientes().size() == tamanoInicial + 1);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("No se pudo agregar el cliente nulo");
        }
    }

    /**
     * CE3: Agregar dos clientes con el mismo documento
     *      TIPO: Frontera. Resultado Esperado: No se agrega
     */ 
    @Test
    public void testCE3() {
        ServiciosAlquilerItemsStub servicios = new ServiciosAlquilerItemsStub();
        try {
            
            Cliente cliente1 = new Cliente("A", 1, "1", "A", "A");
            Cliente cliente2 = new Cliente("A", 1, "1", "A", "A");
            servicios.registrarCliente(cliente1);
            servicios.registrarCliente(cliente2);
            assertTrue("Cliente ya ingresado", servicios.consultarClientes().size() == 1);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(ClientesTest.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    }
    
    /**
     * CE4: p = null TIPO: Frontera. Resultado Esperado: Error
     *  
     */

    
    
    @Test
    public void additems1() throws ExcepcionServiciosAlquiler{
    	
    }
    
    
    
    
    
    
    
}
