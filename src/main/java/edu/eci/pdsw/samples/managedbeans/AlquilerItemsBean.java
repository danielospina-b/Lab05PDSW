/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.pdsw.samples.managedbeans;

import edu.eci.pdsw.samples.entities.Cliente;
import edu.eci.pdsw.samples.entities.Item;
import edu.eci.pdsw.samples.entities.ItemRentado;
import edu.eci.pdsw.samples.services.ExcepcionServiciosAlquiler;
import edu.eci.pdsw.samples.services.ServiciosAlquiler;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author 2106913
 */
@ManagedBean(name = "AlquilerItems")
@SessionScoped
public class AlquilerItemsBean implements Serializable {

    ServiciosAlquiler sp = ServiciosAlquiler.getInstance();
    private long currentClient;

    public AlquilerItemsBean() {
    }
    
    public void registrarCliente(String nombre, long documento, String telefono, String direccion, String email) {
        try {
            sp.registrarCliente(new Cliente(nombre, documento, telefono, direccion, email));
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String goToClientPage(long id) {
        currentClient = id;
        return "RegistroClienteItem";
    }
    
    public Date getCurrentDate() {
        LocalDate localDate = LocalDate.now();
        return java.sql.Date.valueOf(localDate);
    }
    
    public void registrarAlquilerClienteAhora(int id, int numdias) {
        try {
            sp.registrarAlquilerCliente(getCurrentDate(), currentClient, sp.consultarItem(id), numdias);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<ItemRentado> consultarItemsClienteActual() {
        List<ItemRentado> lista = null;
        try {
            lista = sp.consultarItemsCliente(currentClient);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public String getNombreClienteActual() {
        String nombre = null;
        try {
            nombre = sp.consultarCliente(currentClient).getNombre();
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public String getIDClienteActual() {
        return String.valueOf(currentClient);
    }
    
    public List<Cliente> consultarClientes() {
        List <Cliente> lista = null;
        try {
            lista = sp.consultarClientes();
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public long consultarCostoAlquiler(int id, int numdias) {
        long res = 0;
        try {
            res = sp.consultarCostoAlquiler(id, numdias);
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public long consultarMultaAlquiler(int id) {
        long res = 0;
        try {
            res = sp.consultarMultaAlquiler(id, getCurrentDate());
        } catch (ExcepcionServiciosAlquiler ex) {
            Logger.getLogger(AlquilerItemsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    public List<Item> consultarItemsDisponibles() {
        List<Item> lista = null;
        lista = sp.consultarItemsDisponibles();
        return lista;
    }
}